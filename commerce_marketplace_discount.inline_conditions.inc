<?php

/**
 * Implements hook_inline_conditions_info().
 */
function commerce_marketplace_discount_inline_conditions_info() {
  $conditions = array();

  if (module_exists('commerce_marketplace_order')) {
    $conditions['commerce_marketplace_order_compare_order_amount'] = array(
      'label' => t('Marketplace: Total amount'),
      'entity type' => 'commerce_order',
      'callbacks' => array(
        'configure' => 'commerce_marketplace_order_compare_order_amount_configure',
        'build' => 'commerce_marketplace_order_compare_order_amount_build',
      ),
    );
  }

  return $conditions;
}

/**
 * Configuration callback for commerce_order_compare_order_amount.
 *
 * @param array $settings
 *   An array of rules condition settings.
 *
 * @return array;
 *   A form element array.
 *
 * @see commerce_order_compare_order_amount_configure()
 */
function commerce_marketplace_order_compare_order_amount_configure($settings) {
  $form = array();

  // Get the default website currency.
  $default_currency = commerce_currency_load(NULL);

  $form['operator'] = array(
    '#type' => 'select',
    '#title' => t('Operator'),
    '#title_display' => 'invisible',
    '#options' => array(
      '<' => t('lower than'),
      '==' => t('equals'),
      '>' => t('greater than'),
    ),
    '#default_value' => !empty($settings['operator']) ? $settings['operator'] : '>=',
  );

  $form['total'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#element_validate' => array('commerce_price_field_widget_validate'),
    '#suffix' => '<div class="condition-instructions">' . t(
        'The discount is active only if the order total matches the above condition.
              <br />(the other order discounts are not taken into the comparison process)'
      ) . '</div>',
  );

  $form['total']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Order subtotal'),
    '#title_display' => 'invisible',
    '#default_value' => commerce_currency_amount_to_decimal($settings['total']['amount'], $default_currency['code']),
    '#size' => 10,
    '#field_suffix' => $default_currency['code'],
    '#require' => TRUE,
  );

  $form['total']['currency_code'] = array(
    '#type' => 'value',
    '#default_value' => $default_currency['code'],
  );

  return $form;
}
