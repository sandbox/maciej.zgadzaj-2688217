<?php

/**
 * Implements hook_rules_condition_info().
 */
function commerce_marketplace_discount_rules_condition_info() {
  $inline_conditions = inline_conditions_get_info();
  $conditions = array();

  if (module_exists('commerce_marketplace_order')) {
    $conditions['commerce_marketplace_order_compare_order_amount'] = array(
      'label' => t('Marketplace: Order amount comparison'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
          'description' => t('The order.'),
          'wrapped' => TRUE,
        ),
        'operator' => array(
          'type' => 'text',
          'label' => t('Operator'),
          'description' => t('The operator used with the order amount value below.'),
          'default value' => '>=',
          'options list' => '_inline_conditions_order_operator_options',
        ),
        'total' => array(
          'type' => 'commerce_price',
          'label' => t('Order amount'),
          'default value' => '',
          'description' => t('The value to compare against the passed order amount.'),
        ),
      ),
      'module' => 'inline_conditions',
      'group' => t('Commerce Marketplace Order'),
      'callbacks' => array(
        'execute' => $inline_conditions['commerce_marketplace_order_compare_order_amount']['callbacks']['build'],
      ),
    );
  }

  return $conditions;
}

/**
 * Build callback for commerce_marketplace_order_compare_order_amount.
 *
 * Copy of commerce_order_compare_order_amount_build() for marketplace orders.
 *
 * @param EntityDrupalWrapper $wrapper
 *   The wrapped entity given by the rule.
 * @param string $operator
 *   The comparison operator.
 * @param array $total
 *   A commerce_price type array.
 *
 * @return bool
 *   Returns TRUE if condition is valid, FALSE otherwise.
 *
 * @see commerce_marketplace_discount_inline_conditions_info()
 * @see commerce_order_compare_order_amount_build()
 */
function commerce_marketplace_order_compare_order_amount_build(EntityDrupalWrapper $wrapper, $operator, $total) {
  $group_orders_total = 0;

  // Ensure the discount currency code is the same than the order.
  if ($wrapper->commerce_order_total->currency_code->value() != $total['currency_code']) {
    return FALSE;
  }

  $refreshed_original = $refreshed = &drupal_static('commerce_cart_commerce_order_load', array());

  $group_order_ids = db_query('SELECT order_id FROM {commerce_order} WHERE order_group = :order_group', array(':order_group' => $wrapper->value()->order_group))->fetchCol();
  foreach ($group_order_ids as $group_order_id) {
    $refreshed[$group_order_id] = TRUE;
  }

  // Load all orders from the order group and calculate their total.
  $group_orders = commerce_marketplace_order_group_load($wrapper->value()->order_group);

  $refreshed = $refreshed_original;

  foreach ($group_orders as $group_order) {
    $group_order_wrapper = entity_metadata_wrapper('commerce_order', $group_order);
    foreach ($group_order_wrapper->commerce_line_items as $line_item_wrapper) {
      if ($line_item_wrapper->getBundle() != 'commerce_discount') {
        // Convert the line item's total to the order's currency for totalling.
        $component_total = commerce_price_component_total($line_item_wrapper->commerce_total->value());

        // Add the totals.
        $group_orders_total += commerce_currency_convert(
          $component_total['amount'],
          $component_total['currency_code'],
          $total['currency_code']
        );
      }
    }
  }

  switch ($operator) {
    case '<':
      if ($group_orders_total >= $total['amount']) {
        drupal_set_message(t('The total amount of your shopping cart is too high to benefit from this promo code.'), 'error', FALSE);
      }
      return $group_orders_total < $total['amount'];

    case '==':
      if ($group_orders_total != $total['amount']) {
        drupal_set_message(t('The total amount of your shopping cart is not correct to benefit from this promo code.'), 'error', FALSE);
      }
      return $group_orders_total == $total['amount'];

    case '>':
      if ($group_orders_total <= $total['amount']) {
        drupal_set_message(t('The total amount of your shopping cart is not enough to benefit from this promo code.'), 'error', FALSE);
      }
      return $group_orders_total > $total['amount'];

    default:
      return FALSE;
  }
}

/**
 * Implements hook_commerce_discount_offer_type_info_alter().
 *
 * @see commerce_discount_offer_types()
 * @see commerce_discount_commerce_discount_offer_type_info()
 */
function commerce_marketplace_discount_commerce_discount_offer_type_info_alter(&$offer_types) {
  if (!empty($offer_types['fixed_amount'])) {
    $offer_types['fixed_amount']['action'] = 'commerce_marketplace_discount_fixed_amount';
  }
}

/**
 * Rules action: Apply fixed amount discount.
 *
 * Overrides commerce_discount_fixed_amount() for marketplace orders.
 *
 * @see commerce_marketplace_discount_commerce_discount_offer_type_info_alter()
 * @see commerce_discount_fixed_amount()
 */
function commerce_marketplace_discount_fixed_amount(EntityDrupalWrapper $wrapper, $discount_name) {
  // The idea here is: first time this function is called for a specific order
  // group, we calculate discount values for each order and save in the
  // $fixed_amount_discounts static variable. The, for all subsequent calls
  // for other orders from the same order group, we'll use already calculated
  // discount values, instead of re-calculating them each time.
  $fixed_amount_discounts = &drupal_static(__FUNCTION__, array());

  // We'll need few functions from commerce_discount.rules.inc.
  module_load_include('rules.inc', 'commerce_discount');

  // If the wrapper is not an order, we do standard processing.
  if ($wrapper->type() != 'commerce_order') {
    commerce_discount_fixed_amount($wrapper, $discount_name);
    return;
  }

  // Get discount details.
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
  $discount_price = $discount_wrapper->commerce_discount_offer->commerce_fixed_amount->value();

  // We'll calculate total shipping amounts for each order here.
  $discountable_amounts = array();
  // By default, we don't want to recalculate discount amounts.
  $recalculate_discounts = FALSE;

  // We want to load all orders from the order group, to calculate shipping
  // amount for each one, and then related discount amounts. We do not want
  // to refresh them again though when we load them (as most probably we are
  // already within the refresh process), so temporarily we'll mark all
  // the orders from the group as already refreshed.
  $refreshed_original = $refreshed = &drupal_static('commerce_cart_commerce_order_load', array());
  // Get IDs of all the orders from the current order group.
  $group_order_ids = db_query('SELECT order_id FROM {commerce_order} WHERE order_group = :order_group', array(':order_group' => $wrapper->value()->order_group))->fetchCol();
  // Mark them all as already refreshed.
  foreach ($group_order_ids as $group_order_id) {
    $refreshed[$group_order_id] = TRUE;
    // If for any order from the order group we do not have the discount amount
    // calculated yet, we need to do full recalculation.
    if (!isset($fixed_amount_discounts[$group_order_id])) {
      $recalculate_discounts = TRUE;
    }
  }

  // Now that all group orders are marked as 'refreshed', we can load them all
  // without causing them to get refreshed again.
  $group_orders = commerce_marketplace_order_group_load($wrapper->value()->order_group);
  // Revert $refresh to its original value.
  $refreshed = $refreshed_original;

  // It seems that we need to recalculate discount amounts each time an order
  // is refreshed, as otherwise, when for example product quantity is updated
  // on the Shopping cart page, it will generate incorrect amounts.
//  if ($recalculate_discounts) {
    // Get the line items types which discount should be applied to.
    $discounted_line_item_types = variable_get('commerce_discount_line_item_types', array('product'));

    // For each order we need to calculate total discountable amount, which is
    // the total amount of all discountable shipping line items.
    foreach ($group_orders as $group_order) {
      $group_order_discountable_total = 0;

      if (!empty($group_order)) {
        $group_order_wrapper = entity_metadata_wrapper('commerce_order', $group_order);
        foreach ($group_order_wrapper->commerce_line_items as $line_item_wrapper) {
          $line_item_bundle = $line_item_wrapper->getBundle();
          // Process only discountable line item types.
          if (!empty($discounted_line_item_types[$line_item_bundle])) {
            // Convert the line item's total to the order's currency for totalling.
            $component_total = commerce_price_component_total($line_item_wrapper->commerce_total->value());

            // Add the totals.
            $group_order_discountable_total += commerce_currency_convert(
              $component_total['amount'],
              $component_total['currency_code'],
              $group_order_wrapper->commerce_order_total->currency_code->value()
            );
          }
        }
      }

      $discountable_amounts[$group_order->order_id] = $group_order_discountable_total;
    }
    // Order orders by their discountable amounts, in ascending order.
    // The idea here is that when calculated order discount is higher than
    // order discountable amount, the discount amount for this order will be
    // smaller, and the remaining amount will need to be split between
    // remaining orders.
    asort($discountable_amounts);

    // Calculate discount amounts for each order.
    $remaining_discount = $discount_price['amount'];
    $remaining_orders = count($group_orders);
    foreach ($discountable_amounts as $group_order_id => $group_order_discountable_amount) {
      $fixed_amount_discounts[$group_order_id] = $remaining_discount / $remaining_orders;

      if ($fixed_amount_discounts[$group_order_id] > $group_order_discountable_amount) {
        $fixed_amount_discounts[$group_order_id] = $group_order_discountable_amount;
      }

      $remaining_discount -= $fixed_amount_discounts[$group_order_id];
      $remaining_orders--;
    }
//  }

  // Exit if the wrapper doesn't have a commerce_discounts property.
  if (!$wrapper->__isset('commerce_discounts')) {
    return;
  }

  // Set reference to the discount.
  $order = $wrapper->value();

  $discount_price['amount'] = -$fixed_amount_discounts[$order->order_id];

  $delta = $wrapper->commerce_discounts->count();
  $order->commerce_discounts[LANGUAGE_NONE][$delta]['target_id'] = $discount_wrapper->discount_id->value();

  // Modify the existing discount line item or add a new one if that fails.
  if (
    !commerce_discount_set_existing_line_item_price($wrapper, $discount_name, $discount_price)
    // We do not want to add discount line items with amount equal to 0.
    && $discount_price['amount'] < 0
  ) {
    commerce_discount_add_line_item($wrapper, $discount_name, $discount_price);
  }

  // Update the total order price, for the next rules condition (if any).
  commerce_order_calculate_total($order);
}
